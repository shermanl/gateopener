const five = require('johnny-five')
const chipio = require('chip-io')
const express = require('express')
const app = express()

var initalized = false

app.get('/', function (req, res) {
  if (!initalized) {
    return res.sendStatus(500)
  }

  if (req.query.key !== 'Z2JtqeKc88uFu5nMYvQD6JwC') {
    return res.sendStatus(400)
  }

  if (req.query.led !== 'LCD-D4' && req.query.led !== 'LCD-D6' && req.query.led !== 'LCD-D8' && req.query.led !== 'LCD-D10') {
    return res.sendStatus(400)
  }

  const led = new five.Led(req.query.led)
  led.off()
  console.log('Started ', req.query.led);

  setTimeout(function(){
    led.on()
    console.log('Ended ', req.query.led);
    res.sendStatus(200)
  },1500)
})

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})


const board = new five.Board({
  io: new chipio()
})

board.on('ready', function() {
  initalized = true
})
